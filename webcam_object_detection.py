import cv2
import subprocess
from YOLOv6 import YOLOv6

cap = cv2.VideoCapture(0)

# Initialize YOLOv6 object detector
model_path = "D:/diphone/python-itt/best_ckpt.onnx"
yolov6_detector = YOLOv6(model_path, conf_thres=0.7, iou_thres=0.5)

cv2.namedWindow("Detected Objects", cv2.WINDOW_NORMAL)
previous_class_id = None

while cap.isOpened():

    # Read frame from the video
    ret, frame = cap.read()

    if not ret:
        break

    class_names = ['Aku sayang kamu', 'Tidak', 'Tolong', 'Terimakasih']

    # Update object localizer
    boxes, scores, class_ids = yolov6_detector(frame)

    combined_img = yolov6_detector.draw_detections(frame)
    cv2.imshow("Detected Objects", combined_img)

    for class_id in class_ids:
        if class_id != previous_class_id and class_id is not None:
            text = (class_names[class_id])
            try:
                subprocess.run(["wsl", "espeak-ng", "-v", "mb-id1", "-s", "70", text], check=True)
            except subprocess.CalledProcessError as e:
                print("Error:", e)

    # Press key q to stop
    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

# Release the webcam and close windows
cap.release()
cv2.destroyAllWindows()